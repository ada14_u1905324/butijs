define('tools',['public/javascripts/models/card'],function(CardModel) {
    var tools = {};

    tools.distributeCards = function () {
        var deck = tools.mixedDeck();
        var user_decks = new Array();
        for (var i = 0; i < 4; i++) {
            user_decks[i] = new Array();
            for (var j = 0; j < 12; j++) {
                user_decks[i][j] = deck[i * 12 + j];
            }
        }
        return user_decks;
    };

    tools.mixedDeck = function () {
        var deck = new Array();
        var pals = ['Coins', 'Cups', 'Swords', 'Clubs'];
        for (var i = 0; i < 4; i++) {
            for (var j = 0; j < 12; j++) {
                deck[12 * i + j] = new CardModel({suit: pals[i], number: j + 1});
            }
        }
        return tools.shuffle(deck);
    };

    /**
     * Funcio que fa una permutacio aleatoria d'un array
     * Utilitza l'algorisme Fisher-Yates
     * Ref: http://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
     * @param array
     * @returns Permutacio
     */
    tools.shuffle = function (array) {
        var currentIndex = array.length
            , temporaryValue
            , randomIndex
            ;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    }

    return tools;
});

