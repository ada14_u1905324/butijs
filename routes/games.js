module.exports = function(db) {

    var util = require('../util');
    var dao = require('../dao')(db);
    var W = require('when');

    return {
        create : function(req, res) {
            if (!req.body.name) {
                util.stdErr400(res, "Missing 'name' attribute"); return;
            }
            else if (!req.body.user1id) {
                util.stdErr400(res, "Missing 'user1id' attribute"); return;
            }
            else {
                dao.Game.create(req.body)
                    .then(util.stdSeqSuccess.genFuncLeft(res, {}), util.stdErr500.genFuncLeft(res))
                    .done();
            }
        },
        update : function(req, res) {
            if (!req.params.id) {
                util.stdErr400(res, "Missing 'id' attribute"); return;
            }
            else {
                delete req.body.id;
                db.Game.update(req.body,{id:req.params.id});
            }
        },
        getById : function(req,res) {
            db.Game.find(req.params.id).success(function(game){
                res.setHeader('Content-Type','text-json');
                res.end(JSON.stringify(game));
            });
        },
        getList : function(req,res) {
            var ended = req.params.ended==="true";
            var started = req.params.started==="true";
            db.Game.findAll({include: [{model:db.User, as: "user1"}, {model:db.User, as: "user2"}, {model:db.User, as: "user3"}, {model:db.User, as: "user4"}], where : {ended:ended,started:started}}).success(function(game){
                res.setHeader('Content-Type','text-json');
                res.end(JSON.stringify(game));
            });
        }
    }
}