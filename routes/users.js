
/*
 * GET users listing.
 */

/*
exports.list = function(req, res){
  res.send("respond with a resource");
};
*/
module.exports = function(db) {

    var util = require('../util');
    var dao = require('../dao')(db);
    var crypto = require('crypto');

    return {
        create : function(req, res) {
            if (!req.body.username) {
                util.stdErr400(res, "Missing 'username' attribute"); return;
            }
            else if (!req.body.password){
                util.stdErr400(res, "Missing 'password' attribute"); return;
            }
            else if (!req.body.email) {
                util.stdErr400(res, "Missing 'email' attribute"); return;
            }
            else {
                var t;
                dao.User.create(req.body, t)
                    .then(util.stdSeqUserSuccess.genFuncLeft(res, {}), util.stdErr500.genFuncLeft(res))
                    .done();
            }
        },
        getById : function(req,res) {
            db.User.find(req.params.id).success(function(user){
                res.setHeader('Content-Type','text-json');
                res.end(JSON.stringify(user));
            });
        },
        isLoggedIn : function(req,res) {
            res.setHeader('Content-Type','text-json');
            if(req.session.identifier == undefined) {
                res.end(JSON.stringify({isLoggedIn: false}));
            }
            else
                res.end(JSON.stringify({isLoggedIn : true}));
        },
        login : function(req,res){
            db.User.find({where:'username = \'' + req.body.username + '\''}).success(function(user){
                res.setHeader('Content-Type','text-json');
                if(user == null) {
                    res.end(JSON.stringify({success: false}));
                }
                else {
                    var md5 = crypto.createHash('md5');
                    md5.update(req.body.password, 'utf8');
                    var digest = md5.digest('hex');
                    if(digest === user.password) {
                        req.session.identifier = user.id;
                        res.end(JSON.stringify({success: true, user: user}));
                    }
                    else{
                        res.end(JSON.stringify({success : false}));
                    }
                }
            });
        },
        logout : function(req,res){
            res.setHeader('Content-Type','text-json');
            req.session.destroy();
            res.end(JSON.stringify({success:true}));
        }
    }
}