
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/users');
var http = require('http');
var path = require('path');
var connect = require('connect');


var app = express();

var cookieParser = express.cookieParser('ASGSHSDSHEARHH');
app.use(cookieParser);
sessionStore = new connect.middleware.session.MemoryStore();
app.use(express.session({ store: sessionStore }));
//var cookieParser = express.cookieParser();
//var sessionStore = express.session({secret : '1234567890QWERTY'});
//app.use(cookieParser);
//app.use(sessionStore);


// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));



Function.prototype.genFuncLeft = function () {
    var fn = this, args = Array.prototype.slice.call(arguments);
    return function () {
        return fn.apply(null, args.concat(Array.prototype.slice.call(arguments)));
    }
}

Function.prototype.genFuncRight = function () {
    var fn = this, args = Array.prototype.slice.call(arguments);
    return function () {
        return fn.apply(null, Array.prototype.slice.call(arguments).concat(args));
    }
}

var db = require('./models')(app);

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

db.initPromise
    .then(function() {
        var users = require('./routes/users')(db);
        var games = require('./routes/games')(db);

        app.get('/users/:id', users.getById);
        app.post('/login',users.login);
        app.post('/logout',users.logout);
        app.get('/isloggedin',users.isLoggedIn);
        app.post('/users',users.create);

        app.get('/games/:id', games.getById);
        app.put('/games/:id', games.update);
        app.get('/games', games.getList);
        app.post('/games', games.create);

        var port = process.env.OPENSHIFT_NODEJS_PORT || app.get('port');
        var ip = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
        var servergamelogic = require('./servergamelogic');
        var rooms = new Array();
        var server = http.createServer(app).listen(port, ip, function () {
            console.log("Express server listening on " + ip + ":" + port);
            var io = require('socket.io').listen(server);
            var SessionSockets = require('session.socket.io') ,
                sessionSockets = new SessionSockets(io, sessionStore, cookieParser);

            sessionSockets.on('connection', function (err, socket, session) {
                servergamelogic.defineEvents(err,io,socket,session,rooms,db);
            });
        })
    }, function (err) {
        console.log("Error initializing database: " + err);
    })
    .done();

