module.exports = function (){
    var Game = require('./Playing/game');
    var Donat = require('./Playing/donat');
    var Base = require('./Playing/base');
    var Card = require('./Playing/card');

    return {
        defineEvents : function(err,io,socket,session,rooms,db){
            socket.emit('confirm_connection', { success: true });

            socket.on('create_game', function (data) {
                var room = data.gameId;
                rooms[room] = {};
                rooms[room].sockets = new Array();
                rooms[room].sockets[1] = socket;
                socket.roomName = room;
                socket.join(room);
                rooms[room].playerNames = new Array();
                rooms[room].playerNames[1] = data.userName;
                socket.emit('game_joined',{gamePosition: 1, playerNames: rooms[room].playerNames});
            });

            socket.on('join_game', function (data) {
                var userName = data.userName;
                var position = data.gamePosition;
                var room = data.gameId;
                socket.roomName = room;
                rooms[room].sockets[position] = socket;
                rooms[room].playerNames[position] = data.userName;
                socket.join(room);
                socket.emit('game_joined',{gamePosition: position});

                io.sockets.in(room).emit('user_joined_game',{userName: userName, gamePosition: position, playerNames: rooms[room].playerNames});
                var full = true;
                for(var i = 1; i <= 4; i++){
                    if(rooms[room].sockets[i] == undefined){
                        full = false;
                        break;
                    }
                }
                if(full){
                    var pGame = new Game()
                    rooms[room].game = pGame;
                    pGame.begin();
                    io.sockets.in(room).emit('begin_game', {playerNames: rooms[room].playerNames});
                    var donat = pGame.currentDonat;
                    for (var i = 1; i <= 4; i++){
                        rooms[room].sockets[i].emit('new_donat', {cards: donat.cards[i - 1]});
                    }
                    rooms[room].sockets[donat.masterPlayer].emit('select_trumph',{canDelegate:true});
                    db.Game.update({started: true},{id: data.gameId});
                }
            });

            socket.on('trumph_selected',function(data) {
                var roomName = socket.roomName;
                var room = rooms[roomName];
                var game = room.game;
                var donat = game.currentDonat;
                if (data.trumph == 'delegate') {
                    var positionPartner = donat.partnerOfMaster();
                    donat.delegated = true;
                    room.sockets[positionPartner].emit('select_trumph',{canDelegate:false});
                } else {
                    io.sockets.in(roomName).emit('selected_trumph', {trumph: data.trumph, delegated: donat.delegated, master: room.playerNames[donat.masterPlayer]});
                    donat.trumph = data.trumph;
                    if (data.trumph == 'botifarra') {
                        game.factor = 2;
                        io.sockets.in(roomName).emit('factor', {factor: 2});
                        room.sockets[donat.currentBase.turn].emit('give_turn',{cards :donat.validCards()});
                    } else {
                        var contrarPlayer = donat.contrarPlayer();
                        room.sockets[contrarPlayer].emit('decide_contrar',{});
                    }
                }
            });

            socket.on('contrar_decision',function(data){
                var roomName = socket.roomName;
                var room = rooms[roomName];
                var game = room.game;
                var donat = game.currentDonat;
                if(data.decision){
                    var playerRecontrar = donat.recontrarPlayer();
                    room.sockets[playerRecontrar].emit('decide_recontrar',{});
                }
                else{
                    var playerContrar = donat.contrarPlayer();
                    if(playerContrar == -1){
                        var base = donat.currentBase;
                        var turn = base.turn;
                        game.factor = 1;
                        io.sockets.in(roomName).emit('factor', {factor: 1});
                        room.sockets[turn].emit('give_turn',{cards :donat.validCards()});
                    }
                    else{
                        room.sockets[playerContrar].emit('decide_contrar',{});
                    }
                }
            });

            socket.on('recontrar_decision',function(data){
                var roomName = socket.roomName;
                var room = rooms[roomName];
                var game = room.game;
                var donat = game.currentDonat;
                if(data.decision){
                    var playerSantvicentar = donat.santvicentarPlayer();
                    room.sockets[playerSantvicentar].emit('decide_santvicentar',{});
                }
                else{
                    var playerRecontrar = donat.recontrarPlayer();
                    if(playerRecontrar == -1){
                        var base = donat.currentBase;
                        var turn = base.turn;
                        game.factor = 2;
                        io.sockets.in(roomName).emit('factor', {factor: 2});
                        room.sockets[turn].emit('give_turn',{cards :donat.validCards()});
                    }
                    else{
                        room.sockets[playerRecontrar].emit('decide_recontrar',{});
                    }
                }
            });

            socket.on('santvicentar_decision',function(data){
                var roomName = socket.roomName;
                var room = rooms[roomName];
                var game = room.game;
                var donat = game.currentDonat;
                var base = donat.currentBase;
                var turn = base.turn;
                if(data.decision){
                    game.factor = 8;
                    io.sockets.in(roomName).emit('factor', {factor: 8});
                    room.sockets[turn].emit('give_turn',{cards :donat.validCards()});
                }
                else{
                    var playerSantvicentar = donat.santvicentarPlayer();
                    if(playerSantvicentar == -1){
                        game.factor = 4;
                        io.sockets.in(roomName).emit('factor', {factor: 4});
                        room.sockets[turn].emit('give_turn',{cards :donat.validCards()});
                    }
                    else{
                        room.sockets[playerSantvicentar].emit('decide_sanvicentar',{});
                    }
                }
            });

            socket.on('card_thrown',function(data){
                var roomName = socket.roomName;
                var room = rooms[roomName];
                var game = room.game;
                var donat = game.currentDonat;
                io.sockets.in(roomName).emit('card_thrown', {card: data.card, player: donat.currentBase.turn});
                var card = new Card(data.card.suit, data.card.number);
                donat.removeCard(card);
                if(donat.currentBase.throwCard(card)){ //Si queda gent per tirar a la base
                    room.sockets[donat.currentBase.turn].emit('give_turn',{cards :donat.validCards()});
                }else{//Si la base ja s'ha acabat la base (han tirat tots 4)
                    io.sockets.in(roomName).emit('end_base', {});
                    for(var i = 1; i <= 4; i++){
                        donat.currentBase.recollit[i] = false;
                    }
                }
            });

            socket.on('base_recollida',function(data){
                var roomName = socket.roomName;
                var room = rooms[roomName];
                var game = room.game;
                var donat = game.currentDonat;
                var totRecollit = true;
                donat.currentBase.recollit[data.position] = true;
                for(var i = 1; i <= 4; i++){
                    if(!donat.currentBase.recollit[i]){
                        totRecollit = false;
                        break;
                    }
                }
                if(totRecollit) {
                    if (donat.nextBase()) { //Si encara queden bases per jugar
                        room.sockets[donat.currentBase.turn].emit('give_turn', {cards: donat.validCards()});
                    } else { //Si s'ha acabat el donat
                        game.endDonat();
                        io.sockets.in(roomName).emit('end_donat', { score13: game.score13, score24: game.score24});
                        if (game.nextDonat()) { // Si queden donats per jugar
                            for (var i = 1; i <= 4; i++) {
                                room.sockets[i].emit('new_donat', {cards: game.currentDonat.cards[i - 1]});
                            }
                            room.sockets[game.currentDonat.masterPlayer].emit('select_trumph', {canDelegate: true});
                        }
                        else { //Si s'ha acabat la partida
                            io.sockets.in(roomName).emit('end_game', {winner: game.winner()});
                            db.Game.update({ended: true,score13: game.score13, score24: game.score24},{id: roomName});
                        }
                    }
                }
            });


            //Chat
            socket.on('chat_message',function(data){
                var roomName = socket.roomName;
                io.sockets.in(roomName).emit('chat_message', {sender: data.sender, message: data.message});
            });
        }
    }
}();