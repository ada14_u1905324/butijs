module.exports = function (db) {
    var dao = {};
    var util = require('../util');
    var W = require('when');
    dao.create = function (game, t) {
        var df = W.defer();
        db.Game.create(game, util.addTrans(t, {}))
            .success(function(a){
                df.resolve(a)})
            .error(df.reject);
        return df.promise;
    }
    return dao;
}