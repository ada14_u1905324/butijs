
module.exports = function(db){
    var dao = {};
    var W = require('when');

    dao.User = require('./user')(db);
    dao.Game = require('./game')(db);

    dao.create = function(Model,data){
        var df = W.defer();
        Model.create(data).success(df.resolve).error(df.reject);
        return df.promise;
    }
    return dao;
}