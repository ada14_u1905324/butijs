module.exports = function (db) {
    var dao = {};
    var util = require('../util');
    var W = require('when');
    var crypto = require('crypto'), shasum = crypto.createHash('md5');
    dao.create = function (user, t) {
        var df = W.defer();

        shasum.update(user.password);
        user.password = shasum.digest('hex');

        db.User.create(user, util.addTrans(t, {}))
            .success(df.resolve)
            .error(df.reject);
        return df.promise;
    }
    return dao;
}