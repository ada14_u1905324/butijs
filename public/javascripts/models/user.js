define([
    'underscore',
    'backbone'
], function(_, Backbone){
    var UserModel = Backbone.Model.extend({
        urlRoot : '/users'
    });
    // Return the model for the module
    return UserModel;
});