define([
    'underscore',
    'backbone',
    '../../../tools'
], function(_, Backbone, tools){
    var DonatModel = Backbone.Model.extend({
        defaults : {
            cartes : tools.distributeCards()
        }
    });
    // Return the model for the module
    return DonatModel;
});