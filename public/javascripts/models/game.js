define([
    'underscore',
    'backbone'
], function(_, Backbone){
    var GameModel = Backbone.Model.extend({
        defaults : {
            score13 : 0,
            score24 : 0
        },
        urlRoot : '/games',
        nouDonat : function(){
            this.donatActual = new Donat();
            return this.donatActual;
        }
    });
    // Return the model for the module
    return GameModel;
});