define([
    'underscore',
    'backbone'
], function(_, Backbone){
    var CardModel = Backbone.Model.extend();
    // Return the model for the module
    return CardModel;
});