define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone){
    var CardView = Backbone.View.extend({

        initialize: function () {
            this.render();
        },

        render: function() {
            var template = _.template('<img src="images/<%=card.get(\'suit\')%><%=card.get(\'number\')%>.png">',{card : this.model});
            this.$el.html(this.$el.html()+template);
            return this;
        }
    });
    // Our module now returns our view
    return CardView;
});