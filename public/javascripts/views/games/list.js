define([
    'jquery',
    'underscore',
    'backbone',
    '../../collections/games',
    '../../models/user',
    '../../models/game',
    './game',
    'text!/templates/games/list.html',
    '../../global',
    '../../gamelogic'
], function($, _, Backbone, GamesCollection, UserModel, GameModel, GameView, gamesListTemplate, global, gamelogic) {
    var GamesListView = Backbone.View.extend(function() {

        var games = new GamesCollection();
        var ui;

        return {

            initialize: function (params) {
                var that = this;
                games.on('reset', function () {
                    that.render(games)
                })
            },

            render: function (Ui) {
                // Using Underscore we can compile our template with data
                var compiledTemplate = _.template(gamesListTemplate, {items: games});
                // Append our compiled template to this Views "el"
                this.$el.empty().append(compiledTemplate);
                this.games=games;
                if(Ui.loadLogout != undefined){
                    ui = Ui;
                }
                //Join game
                $("#game_rows").children().children().children().children("button").click(function(){
                    var id = $(this).parent().parent().parent().children().first().children().first().html();
                    var nUser = $(this).parent().parent().children().first().html();
                    var game = games.get(id);
                    var user = 'user' + nUser + 'id';
                    game.set(user, global.user.id);
                    game.save();
                    global.loadGames = false;
                    var gameView = new GameView({el: '#main_view', model: game});
                    gameView.user = global.user;
                    gameView.render();
                    gamelogic.setReturnFunction(ui.loadLogout);
                    gamelogic.defineEvents();
                    gamelogic.defineClickEvents();
                    gamelogic.joinGame(global.user.username, game.get('id'), nUser);
                });
            },

            update: function (options, Ui) {
                var op = options || {reset: true};
                games.fetch(op);
                this.render(Ui);
            }
        }
    }());

    // Our module now returns our view
    return GamesListView;
});