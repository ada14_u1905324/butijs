/**
 * Created by Jordi on 23/05/2014.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    '../../models/game',
    'text!/templates/games/game.html',
    '../../global'
], function($, _, Backbone, GameModel, gameTemplate, global) {
    var GameView = Backbone.View.extend(function() {
        return {

            initialize: function (params) {
                var that = this;
                //this.get('game').on('reset', function () {
                //    that.render(game)
                //})
            },

            render: function () {
                // Using Underscore we can compile our template with data
                var compiledTemplate = _.template(gameTemplate, {game: this.model, user: this.user});
                // Append our compiled template to this Views "el"
                this.$el.empty().append(compiledTemplate);
            },
            update: function (options) {
                var op = options || {reset: true};
                this.model.fetch(op);
                this.render();
            }
        }
    }());

    // Our module now returns our view
    return GameView;
});