define([
    'backbone',
    // Pull in the Model module from above
    'models/game'
], function(Backbone, GameModel){
    var GameCollection = Backbone.Collection.extend({
        model: GameModel,
        url: "/games"
    });
    // You don't usually return a collection instantiated
    return GameCollection;
});