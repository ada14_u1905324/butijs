define(['jquery', 'jquery_ui'],function($,jquery_ui) {
    var gameLogic = {};
    var socket;
    var myPosition;
    var enabledCards;
    var playerNames;
    var returnFunction;

    gameLogic.setReturnFunction = function(func){
        returnFunction = function(){
            func();
        }
    }

    gameLogic.defineClickEvents = function(){
        gameLogic.defineTrumphSelecionClick('Coins');
        gameLogic.defineTrumphSelecionClick('Cups');
        gameLogic.defineTrumphSelecionClick('Swords');
        gameLogic.defineTrumphSelecionClick('Clubs');
        gameLogic.defineTrumphSelecionClick('botifarra');
        gameLogic.defineTrumphSelecionClick('delegate');
        gameLogic.defineDoblarClick('contrar');
        gameLogic.defineDoblarClick('recontrar');
        gameLogic.defineDoblarClick('santvicentar');
        $('#btn_tornar').click(returnFunction);
        $('#btn_enviar_msg').click(function(){
            var message = $('#input_msg_chat').val();
            var sender = playerNames[myPosition];
            socket.emit('chat_message', {message: message, sender: sender});
            $('#input_msg_chat').val("");
        });
        $('#input_msg_chat').keypress(function(e){
            if(e.which == 13){
                var message = $('#input_msg_chat').val();
                var sender = playerNames[myPosition];
                socket.emit('chat_message', {message: message, sender: sender});
                $('#input_msg_chat').val("");
            }
        });
        $('#btn_recollir').click(function(){
            $('#user1_ingame').html("");
            $('#user2_ingame').html("");
            $('#user3_ingame').html("");
            $('#user4_ingame').html("");
            $("#dialog-recollir").dialog( "close" );
            socket.emit('base_recollida',{position: myPosition});
        });
    };

    gameLogic.defineTrumphSelecionClick = function(trumph){
        $('#' + trumph + '_trumph').click(function(){
            gameLogic.selectTrumph(trumph);
            $("#dialog-trumfar").dialog( "close" );
        })
    }

    gameLogic.defineDoblarClick = function(action){
        $('#yes_' + action).click(function(){
            socket.emit(action+'_decision',{decision: true});
            $("#dialog-"+action).dialog( "close" );
        })
        $('#no_' + action).click(function(){
            socket.emit(action+'_decision',{decision: false});
            $("#dialog-"+action).dialog( "close" );
        });
    };

    gameLogic.defineCardClick = function(card,selector){
        var obj = $(selector);
        obj.click(function(){
            if(enabledCards[card.suit+card.number]) {
                socket.emit('card_thrown', {card: card});
                obj.remove();
                for(var key in enabledCards){
                    if(key==enabledCards.length || !enabledCards.hasOwnProperty(key)) continue
                    else{
                        enabledCards[key] = false;
                    }
                }

                $('#user1 > img').addClass("disabled");
                $('#user1 > img').removeClass("animation_my_cards");
            }
        });
    };

    gameLogic.selectTrumph = function(trumph){
        socket.emit("trumph_selected", {trumph:trumph});
    }

    gameLogic.createGame = function(userName,gameId){
        socket.emit("create_game", {userName: userName, gameId: gameId});
    };

    gameLogic.joinGame = function(userName,gameId,gamePosition){
        socket.emit("join_game", {userName: userName,gameId: gameId, gamePosition: gamePosition});
    };
    gameLogic.defineEvents = function (){
        socket = io.connect();
        socket.on('confirm_connection', function(data){

        });
        socket.on('game_joined', function (data) {
            myPosition = data.gamePosition; //La meva posicio de la partida
            playerNames = data.playerNames;
        });

        socket.on('user_joined_game', function (data) {
            //Posicio de la partida del nou usuari
            for(var i = 1; i <= 4; i++){
                if(data.playerNames[i] != undefined){
                    var numHtml = (((i - myPosition) + 4) % 4) + 1;
                    var tag = '#user' + numHtml + '_info > .label_player';
                    $(tag).html(data.playerNames[i]);
                }
            }
            playerNames = data.playerNames;
        });

        socket.on('begin_game', function (data) {
            $('#scores_result > tbody > tr:nth-child(1) > th:nth-child(1)').html(playerNames[1] + " - " + playerNames[3]);
            $('#scores_result > tbody > tr:nth-child(1) > th:nth-child(2)').html(":");
            $('#scores_result > tbody > tr:nth-child(1) > th:nth-child(3)').html("0");

            $('#scores_result > tbody > tr:nth-child(2) > th:nth-child(1)').html(playerNames[2] + " - " + playerNames[4]);
            $('#scores_result > tbody > tr:nth-child(2) > th:nth-child(2)').html(":");
            $('#scores_result > tbody > tr:nth-child(2) > th:nth-child(3)').html("0");
        });

        socket.on('new_donat', function (data) {
            enabledCards = new Array();
            for(var i = 0; i < 12; i++)
            {
                var idCard = data.cards[i].suit  + data.cards[i].number;
                var path = "images/" + idCard + ".png"
                $('#user1').append('<img id="'+idCard+'" style="z-index: ' + i + '" class="disabled multiple_cards" src="' + path + '">');
                gameLogic.defineCardClick(data.cards[i],'#'+idCard);
                enabledCards[idCard] = false;

                $('#user3').append('<img class="multiple_cards" src="images/card.png">');
                $('#user4').append('<div class="card_rotateLeft_wrap"><div class="multiple_cards_rotateLeft" style="background: url(\'images/card.png\') no-repeat;background-size: contain;"></div></div>');
                $('#user2').append('<div class="card_rotateRight_wrap"><div class="multiple_cards_rotateRight" style="background: url(\'images/card.png\') no-repeat;background-size: contain;"></div></div>');
            }
        });


        socket.on('select_trumph', function(data){
            if(!data.canDelegate){
                $('#delegate_trumph').css("display","none");
            } else{
                $('#delegate_trumph').css("display","initial");
            }
            $( "#dialog-trumfar").dialog({
                resizable: false,
                height:200,
                width:700,
                modal: true
            });
        });

        socket.on('selected_trumph', function(data){
            $('#scores_info > tbody > tr > td:nth-child(1)').html(data.master);
            $('#scores_info > tbody > tr > td:nth-child(2)').html(data.delegated ? "YES" : "NO");
            $('#scores_info > tbody > tr > td:nth-child(3)').html(data.trumph);
        });

        socket.on('decide_contrar', function (data) {
            $( "#dialog-contrar").dialog({
                resizable: false,
                height:200,
                width:700,
                modal: true
            });

            //var decision; //true, false
            //socket.emit('contrar_decision', {decision: decision});
        });

        socket.on('decide_recontrar', function (data) {
            $( "#dialog-recontrar").dialog({
                resizable: false,
                height:200,
                width:700,
                modal: true
            });
            //var decision; //true, false
            //socket.emit('recontrar_decision', {decision: decision});
        });

        socket.on('decide_santvicentar', function (data) {
            $( "#dialog-santvicentar").dialog({
                resizable: false,
                height:200,
                width:700,
                modal: true
            });
        });

        socket.on('factor', function(data){
            $('#scores_info > tbody > tr > td:nth-child(4)').html("x" + data.factor);
        })

        socket.on('give_turn', function (data) {
            for(var key in data.cards){
                if(key==data.cards.length || !data.cards.hasOwnProperty(key)) continue
                var card = data.cards[key];
                $('#' + card.suit + card.number).addClass("animation_my_cards");
                $('#' + card.suit + card.number).removeClass("disabled");
                enabledCards[card.suit + card.number] = true;
            }
        });

        socket.on('card_thrown', function (data) {
            //data.card.suit;
            //data.card.number;
            var numHtml = (((data.player - myPosition) + 4) % 4) + 1;
            if(numHtml != 1){
                $('#user' + numHtml + ' > img:first').remove();
                $('#user' + numHtml + ' > div:first').remove();
            }
            var idCard = data.card.suit  + data.card.number;
            var path = "images/" + idCard + ".png"
            $('#user1').append('');
            $('#user'+numHtml+'_ingame').html('<img id="'+idCard+'" src="' + path + '">');
        });

        socket.on('end_base', function (data) {
            $("#dialog-recollir").dialog({
                resizable: false,
                height:80,
                width:150,
                modal: true
            });
        });

        socket.on('end_donat', function (data) {
            $('#scores_result > tbody > tr:nth-child(1) > th:nth-child(3)').html(data.score13);
            $('#scores_result > tbody > tr:nth-child(2) > th:nth-child(3)').html(data.score24);

            $('#scores_info > tbody > tr > td:nth-child(1)').html("");
            $('#scores_info > tbody > tr > td:nth-child(2)').html("");
            $('#scores_info > tbody > tr > td:nth-child(3)').html("");
            $('#scores_info > tbody > tr > td:nth-child(4)').html("");
        });

        socket.on('end_game', function (data) {
            var user13 = $('#scores_result > tbody > tr:nth-child(1) > th:nth-child(1)').html();
            var user24 = $('#scores_result > tbody > tr:nth-child(2) > th:nth-child(1)').html();

            if(data.winner == "13"){
                $('#dialog-endGame > p').append(user13);
            }
            else{
                $('#dialog-endGame > p').append(user24);
            }
            $("#dialog-endGame").dialog({
                resizable: false,
                height:200,
                width:700,
                modal: true
            });
        });

        socket.on('chat_message',function(data){
            $('#msg_chat').append("<p><b>" + data.sender + ":</b> " + data.message + "</p>");
        });


    };

    return gameLogic;
});

