define('app', ['jquery', 'backbone', 'ui', 'socket'], function($, Bb, Ui, io) {
    var App = {};
    App.initialize = function() {
        Ui.initialize();
    };
    return App;
});