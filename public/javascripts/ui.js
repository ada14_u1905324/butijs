define(
    [
        'jquery',
        'backbone',
        'views/cards/card',
        'views/games/list',
        'views/games/game',
        'models/card',
        'models/game',
        'global',
        'gamelogic'
    ], function($, Backbone,CardsView,GamesView, GameView, ModelCard, GameModel,  global, gamelogic) {
    var Ui = {};
    var gamesView;
    Ui.initialize = function() {
        Ui.showNav();
    };

    Ui.loadLogout = function(){
        global.loadGames = true;
        $('#navbar_user').html(Ui.logged);
        $('#label_username').html(global.user.username);
        $('#btn_logout').click(function(){
            $.post( "http://localhost:3000/logout")
                .done(function(data) {
                    Ui.loadLoggin();
                });
        });
        $('.navbar-brand').click(function(){
            $('#main_view').html(Ui.home);
        });

        $('#main_view').html('<div id="menu_list_games"></div><div id="list_games"></div>');
        Ui.showMenuListGames();
        gamesView = new GamesView({el: '#list_games'});
        Ui.showGames();
    };

    Ui.loadLoggin = function(){
        global.loadGames = false;
        $('#navbar_user').html(Ui.login);
        $('#btn_login').click(function(){
            var username = $('#input_username').val();
            var password = $('#input_password').val();
            $.post( "http://localhost:3000/login", {username: username, password: password})
                .done(function(data) {
                    if(data.success) {
                        global.user = data.user;
                        Ui.loadLogout();
                    }
                    else
                        Ui.loadLoggin();
                });
        });
        $('#btn-register').click(function(){
            $("#main_view").html(Ui.register);
            $('#btn-register_form').click(function(){
                var username = $('#input_username_form').val();
                var password = $('#input_password_form').val();
                var password_confirm = $('#input_password_confirm_form').val();
                var email = $('#input_email_form').val();
                if(password === password_confirm) {
                    $.post("http://localhost:3000/users", {username: username, password: password, email: email})
                        .done(function (data) {
                                global.user = data.user;
                                Ui.loadLogout();
                        });
                }
            });
        });
        $('#input_password').keypress(function(e){
            if(e.which == 13){
                var username = $('#input_username').val();
                var password = $('#input_password').val();
                $.post( "http://localhost:3000/login", {username: username, password: password})
                    .done(function(data) {
                        if(data.success) {
                            global.user = data.user;
                            Ui.loadLogout();
                        }
                        else
                            Ui.loadLoggin();
                    });
            }
        });
        $('.navbar-brand').click(function(){
            $('#main_view').html(Ui.home);
        });
        $("#main_view").html(Ui.home);
    }

    Ui.showMenuListGames = function (){
        var html =
        '<div id="list_games_header" class="page-header">' +
            '<h1>Games</h1>' +
        '</div>' +
        '<div id="create_game" class="page-header">' +
            '<input id="txt_create_game" type="text" class="form-control" placeholder="Name of the game">' +
            '<button id="btn_create_game" class="btn btn-success">Create game</button>' +
        '</div>';


        $('#menu_list_games').html(html);
        //Create game
        $('#btn_create_game').click(function(){
            global.loadGame = false;
            var game = new GameModel();
            var nameGame = $('#txt_create_game').val();
            game.set('name',nameGame);
            game.set('user1id',global.user.id);
            game.save({},{
                wait:true,
                success:function(model, response) {
                    console.log('Successfully saved!');
                    loadGameView(game);
                    gamelogic.setReturnFunction(Ui.loadLogout);
                    gamelogic.defineEvents();
                    gamelogic.defineClickEvents();
                    gamelogic.createGame(global.user.username, game.get('id'));
                }
            });
        });
    }

    Ui.showGames = function(Ui) {
        return function() {
            if (global.loadGames) {
                var update_time = 2000;
                var options = {reset: true, data: {ended: false, started: false}};
                gamesView.update(options, Ui);
                setTimeout(Ui.showGames, update_time, Ui);
            }
        };
    } (Ui);

    //Show game
    var loadGameView = function(game){
        global.loadGames = false;
        var gameView = new GameView({el: '#main_view', model: game});
        gameView.user = global.user;
        gameView.render();
    };


    Ui.login =
        '<form class="navbar-form navbar-right">'
        + '<div class="form-group"><input id="input_username" type="text" class="form-control" placeholder="Username"></div>'
        + '<div class="form-group"><input id="input_password" type="password" class="form-control" placeholder="Password"></div>'
        + '<input type="button" class="btn btn-danger" value="Login" id="btn_login">'
        + '<input type="button" class="btn btn-primary" value="Registrar" id="btn-register">'
        + '</form>';

    Ui.logged ='<div class="navbar-right"><p id="label_username"></p><input type="button" class="btn btn-danger" value="Logout" id="btn_logout"></div>';

    Ui.home = '<div class="page-header"><h1>Benvinguts a ButiJS</h1></div>' +
        '<div class="container">' +
        '<h3>En aquesta pàgina podràs jugar al joc de la borifarra online amb els teus amics' +
        '</h3></div>';

    Ui.register = '<div class="page-header"><h1>Register</h1></div>' +
        '<form><div class="form-group">' +
        '<input id="input_username_form" type="text" class="form-control" placeholder="Username">' +
        '<input id="input_email_form" type="text" class="form-control" placeholder="E-mail">' +
        '<input id="input_password_form" type="password" class="form-control" placeholder="Password">' +
        '<input id="input_password_confirm_form" type="password" class="form-control" placeholder="Confirm password">' +
        '<input type="button" class="btn btn-primary" value="Registrar" id="btn-register_form">' +
        '</div></form>';
        
    Ui.showNav = function(){
        $.getJSON('http://localhost:3000/isloggedin', function(data){
            if(data.isLoggedIn){
                Ui.loadLogout();
            }
            else {
                Ui.loadLoggin();
            }
        });
    };
    return Ui;
});
