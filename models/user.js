/**
 * Created by Marc on 31/03/2014.
 */
/**
 * New node file
 */

module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define('User', {
        username : DataTypes.STRING(255),
        password : DataTypes.STRING(260),
        email : DataTypes.STRING(255),
        createdAt : {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: new Date()
        },
        updatedAt : {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: new Date()
        }
    }, {
        classMethods : {
            associate : function(models) {
            }
        }});

    return User;
};