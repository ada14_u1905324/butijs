/**
 * Created by Jordi on 07/04/2014.
 */
/**
 * Una partida (Game) representa una partida sencera (que acaba passats els 100 punts)
 * Conte varies tirades
 */

module.exports = function(sequelize, DataTypes) {
    var Game = sequelize.define('Game', {
        name : DataTypes.STRING(260),
        score13 : {type : DataTypes.INTEGER, defaultValue:0},
        score24 : {type : DataTypes.INTEGER, defaultValue:0},
        ended :  {type : DataTypes.BOOLEAN, defaultValue:false},
        started : {type : DataTypes.BOOLEAN, defaultValue:false},
        createdAt : {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: new Date()
        },
        updatedAt : {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: new Date()
        }
    }, {
        classMethods : {
            associate : function(models) {
                Game.belongsTo(models.User, {as: 'user1', foreignKey: 'user1id'});
                Game.belongsTo(models.User, {as: 'user2', foreignKey: 'user2id'});
                Game.belongsTo(models.User, {as: 'user3', foreignKey: 'user3id'});
                Game.belongsTo(models.User, {as: 'user4', foreignKey: 'user4id'});
            }
        }
    });

    return Game;
};