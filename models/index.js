
module.exports = function (app) {

    var fs = require('fs');
    var path = require('path');
    var Sequelize = require('sequelize');
    var http = require('http')
    var console = require('console');
    var W = require('when');

    var db = {};

    var User = require('./user');

    console.log("Configuring app for environment: " + app.get('env'));

        var db_credentials = {
            dbname: "butijsdb",
            username: "buti",
            password: "buti",
            host: "127.0.0.1",
            port: 3306
        }
        var force = true;

    console.log("Connecting to " + db_credentials.dbname);

    var sequelize = require('sequelize'), sequelize = new Sequelize(
        db_credentials.dbname, db_credentials.username,
        db_credentials.password, {
            host: db_credentials.host,
            dialect: "mysql",
            port: db_credentials.port,
            logging: console.log
        });

    db.Sequelize = Sequelize;
    db.sequelize = sequelize;

    var df = W.defer();
    db.initPromise = df.promise;

    sequelize.authenticate().complete(function (err) {
        if (!!err) {
            df.reject(err);
        } else {
            console.log('Connection has been established successfully.');
            fs.readdirSync(__dirname).filter(function (file) {
                return (file.indexOf('.') !== 0) && (file !== 'index.js')
            }).forEach(function (file) {
                var model = sequelize.import(path.join(__dirname, file))
                db[model.name] = model;
            });

            Object.keys(db).forEach(function (modelName) {
                if ('associate' in db[modelName]) {
                    db[modelName].associate(db);
                }
            });

            sequelize
                .sync({ force: force })
                .complete(function (err) {
                    if (err) {
                        df.reject(err)
                    } else {
                        fs.readFile('initdb.sql','utf8',function(err,data){
                            if(err) {
                                return console.log(err);
                            }
                            var queries = data.split(';');
                            var runQueries = function(queries){
                                if(arguments.length > 0){
                                    sequelize
                                        .query(queries.shift())
                                        .success(function(result){
                                            df.resolve();;
                                            runQueries(queries);
                                        });
                                }
                            }
                            runQueries(queries);
                        });
                    }
                })
        }
    });

    return db;
}