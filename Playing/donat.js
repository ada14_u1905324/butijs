/**
 * Created by Jordi on 05/06/2014.
 */

module.exports = function () {
    var Base = require('./base');
    var Card = require('./card');

    var Donat = function (masterPlayer) {
        this.masterPlayer = masterPlayer;
        this.cards = distributeCards();
        this.currentBase = new Base((masterPlayer % 4) + 1);
        this.playedBases = 0;
        this.delegated = false;
        this.contrarAskedTimes = 0;
        this.recontrarAskedTimes = 0;
        this.santvicentarAskedTimes = 0;

        this.score13 = 0;
        this.score24 = 0;

        //Retorna cert si i nomes si queden bases per jugar
        this.nextBase = function (card) {
            this.playedBases++;
            if(this.currentBase.winner(this.trumph) % 2 == 0)
                this.score24 += this.currentBase.score;
            else
                this.score13 += this.currentBase.score;
            if (this.playedBases == 12) {
                return false;
            } else {
                this.currentBase = new Base(this.currentBase.winner(this.trumph));
                return true;
            }
        };

        this.removeCard = function (card){
            var turn = this.currentBase.turn;
            for (var i=0; i<this.cards[turn-1].length; i++){
                if(this.cards[turn-1][i].number == card.number && this.cards[turn-1][i].suit == card.suit){
                    this.cards[turn-1].splice(i,1);
                    return;
                }
            }
        }

        this.validCards = function(){
            var base = this.currentBase;
            var playerCards = this.cards[base.turn-1];
            if(base.hasNoCards()){ //Si es el primer judador a tirar
                return playerCards; //Pot tirar qualsevol carta
            }else{ //Si no es el primer jugador a tirar
                var firstSuit = base.cards[0].suit;
                var cardsFirstSuit = getCardsOfSuit(firstSuit,playerCards);
                if(cardsFirstSuit.length == 0){//Si no te cartes del primer pal
                    if(base.isWinningPartner(this.trumph)){ //Si esta guanyant el company
                        return playerCards; //Pot tirar qualsevol carta //OK
                    } else { //Si esta guanyant el rival
                        var winnerCards = getWinnerCards(base,playerCards,this.trumph);
                        if(winnerCards.length == 0) { //Si no pot matar
                            return playerCards; //Pot tirar qualsevol carta  //OK
                        } else {
                            return winnerCards; //Pot tirar nomes les cartes que maten //OK
                        }
                    }
                } else { //Si te cartes del primer pal
                    if(base.isWinningPartner(this.trumph)){ //Si esta guanyant el company
                        return cardsFirstSuit; //Pot tirar qualsevol carta del primer pal
                    } else { //Si esta guanyant el rival
                        var winnerCards = getWinnerCards(base,cardsFirstSuit,this.trumph);
                        if(winnerCards.length == 0) { //Si no pot matar amb les cartes del primer pal
                            return [lowestCardInNumber(cardsFirstSuit)]; //Ha de tirar la carta mes petita le primer pal
                        } else {
                            return winnerCards; //Pot tirar nomes les cartes del primer pal que maten
                        }
                    }
                }
            }
        };

        this.partnerOfMaster = function(){
            return ((this.masterPlayer+1 ) % 4) +1;
        };

        this.contrarPlayer = function(){
            switch(this.contrarAskedTimes){
                case 0:
                    //player dreta master
                    this.contrarAskedTimes++;
                    return (this.masterPlayer % 4) +1;
                    break;
                case 1:
                    //player esquerra master
                    this.contrarAskedTimes++;
                    return ((this.masterPlayer+2 ) % 4) +1;
                    break;
                default:
                    return -1;
            }
        };

        this.recontrarPlayer = function(){
            switch(this.recontrarAskedTimes){
                case 0:
                    //master player
                    this.recontrarAskedTimes++;
                    return this.masterPlayer;
                    break;
                case 1:
                    //company master
                    this.recontrarAskedTimes++;
                    return ((this.masterPlayer+1 ) % 4) +1;
                    break;
                default:
                    return -1;
            }
        };

        this.santvicentarPlayer = function(){
            switch(this.santvicentarAskedTimes){
                case 0:
                    //player dreta master
                    this.santvicentarAskedTimes++;
                    return (this.masterPlayer % 4) +1;
                    break;
                case 1:
                    //player esquerra master
                    this.santvicentarAskedTimes++;
                    return ((this.masterPlayer+2 ) % 4) +1;
                    break;
                default:
                    return -1;
            }
        };

    };

    var getWinnerCards = function(base,cards,trumph){
        if(base.cards.length==0){
            return cards;
        }
        else {
            var winner = base.getWinningCard(trumph);
            var winnerCards = new Array();
            var suitFirstCard = base.cards[0].suit;
            for (var i = 0; i < cards.length; i++) {
                if (cards[i].wins(winner, suitFirstCard, trumph)){
                    winnerCards.push(cards[i]);
                }
            }
            return winnerCards;
        }
    };

    var lowestCardInNumber = function(cards){
        var lowest = cards[0];
        for(var i = 1; i < cards.length; i++){
            if(cards[i].lowerInNumber(lowest)){
                lowest = cards[i];
            }
        }
        return lowest;
    }

    var getCardsOfSuit = function(suit,cards){
        var cardsOfSuit = new Array();
        for(var i = 0; i < cards.length; i++){
            if(cards[i].suit == suit){
                cardsOfSuit.push(cards[i]);
            }
        }
        return cardsOfSuit;
    };

    //Aquestes propietats serveixen per definir l'odre de les cartes, per mostrar-les ordenadament
    //Com mes gran el numero, mes gran la preferencia
    var suitPreference = new Array();
    suitPreference['Coins'] = 4;
    suitPreference['Cups'] = 3;
    suitPreference['Swords'] = 2;
    suitPreference['Clubs'] = 1;
    var numberPreference = new Array();

    numberPreference[9] = 12;numberPreference[1] = 11;numberPreference[12] = 10;
    numberPreference[11] = 9;numberPreference[10] = 8;numberPreference[8] = 7;
    numberPreference[7] = 6;numberPreference[6] = 5;numberPreference[5] = 4;
    numberPreference[4] = 3;numberPreference[3] = 2;numberPreference[2] = 1;

    var distributeCards = function() {
        var deck = mixedDeck();
        var user_decks = new Array();
        for (var i = 0; i < 4; i++) {
            user_decks[i] = new Array();
            for (var j = 0; j < 12; j++) {
                user_decks[i][j] = deck[i * 12 + j];
            }
            user_decks[i].sort(function(card1,card2){
                if(suitPreference[card1.suit] == suitPreference[card2.suit]){
                    return numberPreference[card2.number] - numberPreference[card1.number];
                } else{
                    return suitPreference[card2.suit] - suitPreference[card1.suit];
                }
            });
        }
        return user_decks;
    };

    var mixedDeck = function() {
        var deck = new Array();
        var pals = ['Coins', 'Cups', 'Swords', 'Clubs'];
        for (var i = 0; i < 4; i++) {
            for (var j = 0; j < 12; j++) {
                deck[12 * i + j] = new Card(pals[i], j + 1);
            }
        }
        return shuffle(deck);
    };

    /**
     * Funcio que fa una permutacio aleatoria d'un array
     * Utilitza l'algorisme Fisher-Yates
     * Ref: http://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
     * @param array
     * @returns Permutacio
     */
    var shuffle = function (array) {
        var currentIndex = array.length
            , temporaryValue
            , randomIndex
            ;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    };

    return Donat;
}();


