/**
 * Created by Jordi on 05/06/2014.
 */

module.exports = function () {
    var Card = require('./card');

    var Base = function (turn) {
        this.starter = turn;
        this.turn = turn;
        this.cards = new Array();
        this.score = 1;
        this.recollit = new Array();

        //Retorna quin jugador ha guanyat la base
        this.winner = function (trumph) {
            var winningIndex = this.getWinningIndex(trumph);
            return (((this.starter -1) + winningIndex) % 4) +1;
        };

        //Retorna cert si i nomes si queden cartes per tirar a la base
        this.throwCard = function (card) {
            this.cards.push(card);
            this.turn = (this.turn % 4) + 1;
            this.score += card.score();
            return this.cards.length != 4;
        };


        this.isWinningPartner = function(trumph){
            var winningIndex = this.getWinningIndex(trumph);
            var partner = ((this.cards.length+2) % 4);
            return winningIndex == partner;
        };

        this.hasNoCards = function(){
            return this.cards.length == 0;
        };

        this.getWinningCard = function(trumph){
            return this.cards[this.getWinningIndex(trumph)];
        }

        this.getWinningIndex = function(trumph){
            var winningIndex = 0;
            var winningCard = this.cards[0];
            var firstSuit = this.cards[0].suit;
            for(var i = 1; i < this.cards.length; i++){
                if(this.cards[i].wins(winningCard,firstSuit,trumph)){
                    winningCard = this.cards[i];
                    winningIndex = i;
                }
            }
            return winningIndex;
        }

    };



    return Base;
}();