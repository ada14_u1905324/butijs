/**
 * Created by Jordi on 05/06/2014.
 */

module.exports = function () {
    return function (suit, number) {
        this.suit = suit;
        this.number = number;

        this.score = function(){
            switch(this.number){
                case 9:  return 5;
                case 1:  return 4;
                case 12: return 3;
                case 11: return 2;
                case 10: return 1;
                default: return 0;
            }
        }

        this.lowerInNumber = function(card){
            if(this.number == 9){
                return false;
            } else if(card.number == 9){
                return true;
            } else if(this.number == 1){
                return false;
            } else if(card.number == 1){
                return true;
            } else {
                return this.number < card.number;
            }
        };

        this.wins = function(card,firstSuit,trumph)
        {
            if(this.suit == card.suit){
                return card.lowerInNumber(this);
            } else{
                if(this.suit == trumph) {
                    return true;
                } else if(card.suit == trumph) {
                    return false;
                } else if(this.suit == firstSuit){
                    return true;
                } else if(card.suit == firstSuit){
                    return false;
                } else{
                    return false; //Aquest cas es indiferent, ja que cap de les dues cartes son guanyadores a la base
                }
            }
        }
    };
}();