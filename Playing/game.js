/**
 * Created by Jordi on 05/06/2014.
 */
module.exports = function (){
    var Donat = require('./donat');

    var endPunctuation = 101;

    return function () {
        this.currentDonat = new Donat(Math.floor(Math.random() * 4) + 1);
        this.begin = function () {
            this.score13 = 0;
            this.score24 = 0;
        };
        this.endDonat = function(){
            var score = this.currentDonat.score13 - 36;
            if(score > 0){
                this.score13 += this.factor * score;
            } else {
                this.score24 += this.factor * (-score);
            }
        };
        this.nextDonat = function () {
            if(this.score13 >= endPunctuation || this.score24 >= endPunctuation){
                return false;
            }else {
                this.currentDonat = new Donat((this.currentDonat.masterPlayer % 4) + 1)
                return true;
            }
        };

        this.winner = function(){
            if(this.score13 >= endPunctuation)
                return '13';
            else if(this.score24 >= endPunctuation)
                return '24';
            else
                return '';
        };
    };
}();